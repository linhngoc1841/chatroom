<%-- 
    Document   : forgetpwd
    Created on : 27-Feb-2016, 10:49:37 AM
    Author     : Linh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div id="wrapper">
        <jsp:include page="../include/header.jsp" />
        <main>
            <form:form action="sendpwd.htm" commandName="user" method="post">
            <div class="error">${error}</div>
            <div class="info">${info}</div>
            <table>
                <tr>
                    <td>User Name :</td>
                    <td><form:input path="email" maxlength="100" size="40" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" value="Submit"/>&nbsp;
                    </td>
                </tr>
            </table>           
            </form:form>
        </main>
        <jsp:include page="../include/footer.jsp" />
        </div>
    </body>
</html>
