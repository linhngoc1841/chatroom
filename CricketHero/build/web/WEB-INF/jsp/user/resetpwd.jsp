<%-- 
    Document   : resetpwd
    Created on : 27-Feb-2016, 1:50:38 PM
    Author     : Linh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <div id="wrapper">
        <jsp:include page="../include/header.jsp" />
        <main>
            <form:form action="resetpwd.htm" method="post">
            <input type="hidden" name="id" value="${id}"/>
            <div class="error">${error}</div>
            <div class="info">${info}</div>
            <table>
                    <tr>
                            <td>New Password :</td>
                            <td><input type="password" name="password" size="40"/></td>
                    </tr>
                    <tr>
                            <td>Retype Password :</td>
                            <td><input type="password" name="retypePassword" size="40"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Submit"></td>
                    </tr>
            </table>
            </form:form>
        </main>
        <jsp:include page="../include/footer.jsp" />
        </div>
    </body>
</html>
