/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entities.User;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

/**
 *
 * @author Linh
 */
public class BaseDAO<T, ID extends Serializable> extends HibernateDaoSupport{
    private Class<T> persistentClass;

    public BaseDAO() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        
    }
    
    public Class<T>  getPersistentClass() {
        return persistentClass;
    }
    public T save(T obj) {
        try {
            Session session = getSessionFactory().openSession();
            session.setFlushMode(FlushMode.AUTO);
            session.save(obj); 
            session.flush();
            session.close();
        } catch (DataAccessException e) {
            logger.error("save entity error", e);	
        }
        return obj;
    }
    
    public T saveOrUpdate(T obj) {
       Session session = getSessionFactory().openSession();
        session.setFlushMode(FlushMode.AUTO);
        session.saveOrUpdate(obj);
        session.flush();
        session.close();
        return obj;
    }
    
    public void delete(T obj) {
         try {
            Session session = getSessionFactory().openSession();
            session.setFlushMode(FlushMode.AUTO);
            session.delete(obj); 
            session.flush();
            session.close();
        } catch (DataAccessException e) {
            logger.error("save entity error", e);	
        }
                
    }

    public void deleteById(ID id) 	{
        delete(getById(id));
    }
    
    public T getById(ID id) {
        return (T) getHibernateTemplate().get(getPersistentClass(), id);
        
    }
    
    public List<T> getBy(String name,String value){
        Criterion[]  cr = new Criterion[1] ;
        cr[0]=Restrictions.eq(name, value);
        List<T> objs = findByCriteria(cr);
        if(objs.isEmpty())
            return null;
        return objs;
    }
    
    public List<T>  getAll() {
        Session session = getSessionFactory().openSession();
        Criteria crit = session.createCriteria(getPersistentClass());
        List<T>  objs = crit.list();
        session.close();
        return objs;
    }   
    
    public List<T>  getWithSQL(String sql) {
        Session session = getSessionFactory().openSession();
        List<T>  objs = session.createQuery(sql).list();
        session.close();
        return objs;
    } 
    
    public List<T> findByCriteria(Criterion... criterion) {
        Session session = getSessionFactory().openSession();
        Criteria crit = session.createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        List<T> objs = crit.list();
        session.close();
        return objs;
   }
}