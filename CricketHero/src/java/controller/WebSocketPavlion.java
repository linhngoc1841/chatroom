/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author Linh
 */
@ServerEndpoint("/pavilion")
public class WebSocketPavlion {
    static Set<Session> chatroomUsers = Collections.synchronizedSet(new HashSet<Session>());
    @OnOpen
    public void open(Session session) throws IOException {
        chatroomUsers.add(session);
    }

    @OnClose
    public void close(Session session) throws IOException {
        chatroomUsers.remove(session);
        Iterator<Session> iterator = chatroomUsers.iterator();
        while (iterator.hasNext()){
            iterator.next().getBasicRemote().sendText(buildJsonUserData());   
        }
    }
    
    @OnMessage
    public void handleMessage(String message, Session session) throws IOException {
        String username = (String) session.getUserProperties().get("username");        
        if(username==null){
            session.getUserProperties().put("username", message); 
            Iterator<Session> iterator = chatroomUsers.iterator();
            while (iterator.hasNext()){
                iterator.next().getBasicRemote().sendText(buildJsonUserData());   
            }
        }else{            
            Iterator<Session> iterator = chatroomUsers.iterator();
            while (iterator.hasNext()){
                iterator.next().getBasicRemote().sendText(buildJsonData(username, message));   
            }
        }
    }
    
    private String buildJsonData(String username, String message){
        JsonObject jsonObject = Json.createObjectBuilder().add("message", username + ": " + message).build();
        StringWriter stringWriter = new StringWriter();
        try (JsonWriter jsonWriter = Json.createWriter(stringWriter)){
            jsonWriter.write(jsonObject);
        }
        return stringWriter.toString();        
    }
    
    private String buildJsonUserData(){
        Iterator<String> iterator = getUserNames().iterator();
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        while (iterator.hasNext())
            arrayBuilder.add((String)iterator.next());
        return Json.createObjectBuilder().add("users",arrayBuilder).build().toString();
    }
    
    private Set<String> getUserNames(){
        HashSet<String> returnSet = new HashSet<String>();
        Iterator<Session> iterator = chatroomUsers.iterator();
        while (iterator.hasNext())
            returnSet.add(iterator.next().getUserProperties().get("username").toString());
        return returnSet;
    }
    
}
