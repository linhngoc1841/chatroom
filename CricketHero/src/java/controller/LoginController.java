/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Utilities.Utilities;
import entities.User;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import model.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Linh
 */
@Controller
public class LoginController{ 

    @Autowired private UserDAO userDAO;
   
    @RequestMapping(value="/login.htm", method = RequestMethod.GET)
    public String login(ModelMap modelMap) {
        modelMap.put("user", new User());
        return "user/login";
    }
    @RequestMapping(value="/loginConfirm.htm", method = RequestMethod.POST)
    public String loginConfirm(HttpServletRequest request, ModelMap modelMap, User user) {
        if(userDAO.checkUserLogin(user)){
            request.getSession().setAttribute("user", user);
            return "index";
        }
        modelMap.put("error","Username or Password is invalid");
        return "user/login";
    }
    
    @RequestMapping(value="/logout.htm", method = RequestMethod.GET)
    public String logout(HttpServletRequest request,ModelMap modelMap) {
        request.getSession().removeAttribute("user");
        return "index";
    }
    
    @RequestMapping(value="/forgetpwd.htm", method = RequestMethod.GET)
    public String forgetpwd(ModelMap modelMap) {  
        modelMap.put("user", new User());
        return "user/forgetpwd";
    }
    
    @RequestMapping(value="/sendpwd.htm", method = RequestMethod.POST)
    public String sendpwd(HttpServletRequest request,ModelMap modelMap, User user) {      
        if("".equals(user.getEmail())){
            modelMap.addAttribute("error", "Please enter your email");
            return "user/forgetpwd";
        }else{
            List<User> users = userDAO.getBy("email", user.getEmail());
            if(users == null || users.isEmpty()){
                modelMap.addAttribute("error", "This username is invalid. Please try another one");
                return "user/forgetpwd";
            }else{
                user = users.get(0);
            }
        }
        
        String message = "Please click the link below to reset your password \n";
        message += "http://" + request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + "/resetpwdForm.htm?a=" + user.getEmail() + "&p=" + user.getPwd();
        try {
            Utilities.sendEmail(user.getEmail(),"Reset Your Password",message);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(RegistrationController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(RegistrationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        modelMap.addAttribute("info", "Please go to your email to reset your password");
        return "user/forgetpwd";
    }
    
    @RequestMapping(value="/resetpwdForm.htm", method = RequestMethod.GET)
    public String resetpwdForm(HttpServletRequest request, ModelMap modelMap) {  
        List<User> users = userDAO.getBy("email", request.getParameter("a"));       
        /*Check link*/
        if(users == null || users.isEmpty()){
            modelMap.addAttribute("error", "This link is invalid.");
            return "user/resetpwd";
        }
       
        User user = users.get(0);
        if(!user.getPwd().equals(request.getParameter("p"))){
            modelMap.addAttribute("error", "This link is invalid.");
            return "user/resetpwd";
        }
        modelMap.put("id", user.getId());
        return "user/resetpwd";
    }
    
    @RequestMapping(value="/resetpwd.htm", method = RequestMethod.POST)
    public String resetpwd(HttpServletRequest request,ModelMap modelMap) {
        User user = userDAO.getById(Integer.parseInt(request.getParameter("id")));
        String password = request.getParameter("password");
        String reTypePassword = request.getParameter("retypePassword");
        /* validate */
        String error="";
        if("".equals(password)){
            error += "Please enter your new password<br/>";
        }else{            
            if("".equals(reTypePassword)){
                error += "Please enter your retype password<br/>";
            }else{
                if(!reTypePassword.equals(password)){
                     error += "Password is not match with retype password<br/>";
                }
            }           
        }
        
        if(!"".equals(error)){
            modelMap.addAttribute("error", error);
            return "user/resetpwd";
        }
        
        user.setPwd(DigestUtils.md5DigestAsHex(password.getBytes()));
        userDAO.saveOrUpdate(user);
        modelMap.addAttribute("info", "Your password is changed");
        return "user/resetpwd";
    }
}
