<%-- 
    Document   : Pavilion
    Created on : 6-Mar-2016, 4:04:18 PM
    Author     : Linh
--%>
<%@page import="entities.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%  
        String email="";
        User user = (User)session.getAttribute("user");
        if(session.getAttribute("user")!= null){ 
            email = user.getEmail();
        }
%>   
<html>
    <head>
        <title>Pavilion</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            websocket = new WebSocket("ws://localhost:8080/CricketHero/pavilion");
            websocket.onmessage = function processMessage(message){
                var jsonData = JSON.parse(message.data);
                if(jsonData.message != null) 
                    messagesTextArea.value += jsonData.message + "\n";
                if(jsonData.users != null){
                    members.value = "";
                    var i=0;
                    while(i<jsonData.users.length)
                        members.value += jsonData.users[i++] + "\n";
                }
            }
            websocket.onopen = function()
            {
                websocket.send("<%=email%>");
            }
            function sendMessage(){
                websocket.send(messageText.value);
                messageText.value = "";
            } 
        </script>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <div id="wrapper">
        <jsp:include page="../include/header.jsp" />
        <main>
            <div class="chat_message"><textarea id="members" readonly="readonly" rows="22" cols="30"></textarea><br/></div>
            <div>
                <textarea id="messagesTextArea" readonly="readonly" rows="20" cols="45"></textarea><br/>
                <input type="text" id="messageText" size="37"/>
                <input type="button" value="Send" onclick="sendMessage();"/>
            </div>
        </main>
        <jsp:include page="../include/footer.jsp" />
        </div>
    </body>
</html>